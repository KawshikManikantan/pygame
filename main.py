import pygame
import random
import math
from config import *


# icon belongs to turkkub
# croc belongs to Freepik
# rocks belong to Freepik
# defining the class Game
class Game:
    # Originally declaring for the first game function
    def __init__(self):  # constructor for initializing the code
        pygame.mixer.init(44100, 16, 2, 4096)  # music file initiated
        pygame.init()
        self.finish = False
        self.screen = pygame.display.set_mode((800, 780))
        pygame.display.set_caption("Boat race")
        self.icon = pygame.image.load('boat.png')
        pygame.display.set_icon(self.icon)
        self.clock = pygame.time.Clock()
        self.scorereduce, self.t, self.trail = pygame.USEREVENT + 1, 3000, []
        self.done = False
        self.game_over = 0
        self.level = 1
        self.playing = True
        self.player1 = Player(370, 740, 1, "Player1")  # Initializing Player 1
        self.player1.change()
        self.player2 = Player(370, 0, 0, "Player2")  # Initializing Player 2
        self.player2.change()
        # initiating lists of stones and their X and Y coordinates
        self.stoneImg = []
        self.stoneX = []
        self.stoneY = []
        # initiating lists of crosses and their X and Y coordinates
        self.danger = []
        self.dangerx = []
        self.dangery = []
        # Initiating lists of crocodiles with their X,Y coordinates and the
        # amount of change in X axis for the movement of the enemy
        self.crocImg = []
        self.crocx = []
        self.crocy = []
        self.crocxchange = []
        self.count = 0
        self.mainplay = self.player1
        self.collision = 0

    def player(self, name):
        self.screen.blit(name.boatimg, (name.posx, name.posy))

    # bliting the stones all at once
    def stone(self):
        for i in range(24):
            self.screen.blit(self.stoneImg[i],
                             (self.stoneX[i], self.stoneY[i]))

    # bliting the crocs all at once
    def croc(self):
        for i in range(5):
            self.screen.blit(self.crocImg[i], (self.crocx[i], self.crocy[i]))

    # bliting the crosses all at once
    def dang(self):
        for i in range(6):
            self.screen.blit(self.danger[i],
                             (self.dangerx[i], self.dangery[i]))

    # drawing the platforms
    def recta(self):
        color = (150, 0, 0)
        for i in range(4):
            pygame.draw.rect(self.screen, color,
                             pygame.Rect(0, 132 + 162 * i, 800, 30))

    # drawing the scores on the screen
    def score(self, playermain, x, y, x1, y1):
        font = pygame.font.Font('freesansbold.ttf', 32)
        score = font.render("Score : " + str(self.player1.score), True, WHITE)
        score1 = font.render("Score : " + str(self.player2.score), True, WHITE)
        playernow = font.render(
            str(playermain.name) + " - Level " + str(playermain.level), True,
            WHITE)
        self.screen.blit(playernow, (250, 10))
        self.screen.blit(score, (x, y))
        self.screen.blit(score1, (x1, y1))

    # collision algorithm based on distance formula
    # If distance between the objects and the enemies is less than
    # a particular value...Collision is detected
    def collide(self, crocx, crocy, dist):
        distance = math.sqrt(
            math.pow(crocx - self.mainplay.posx, 2) + math.pow(
                crocy - self.mainplay.posy, 2))
        if distance < dist:
            return True
        else:
            return False

    # defining a new_game class that helps us in the restarting the game once
    # enter is pressed
    def new_game(self):
        # initialized
        # print("Heys")
        # self.screen.fill((255,255,255))
        pygame.mixer.music.load(back_music())
        pygame.mixer.music.set_volume(vol())
        pygame.mixer.music.play(-1)

        self.scorereduce, self.t, self.trail = pygame.USEREVENT + 1, 3000, []
        pygame.time.set_timer(self.scorereduce, self.t)
        self.done = False
        self.game_over = 0
        self.level = 1
        self.player1 = Player(370, 740, 1, "Player1")
        self.player1.change()
        self.player2 = Player(370, 0, 0, "Player2")
        self.player2.change()
        self.playing = True

        for i in range(6):
            for j in range(4):
                self.stoneImg.append(pygame.image.load('stones.png'))
                self.stoneX.append(140 * i)
                self.stoneY.append(740 - (142 + 162 * j))

        for i in range(3):
            self.danger.append(pygame.image.load('danger.jpg'))
            self.danger.append(pygame.image.load('danger.jpg'))
            self.dangerx.append(130 * (i + 1) - 50)
            if i == 0:
                self.dangerx.append(750)
            else:
                self.dangerx.append(780 - 130 * (i + 1) + 50)
            self.dangery.append(740 - ((162 * (i + 1))))
            self.dangery.append(740 - (162 * (i + 1)))

        for i in range(5):
            self.crocImg.append(pygame.image.load('crocodile.png'))
            self.crocx.append(random.randint(0, 736))
            self.crocy.append(50 + 150 * i)
            self.crocxchange.append(3)
            self.count = 0
        self.newlevel()

    # defining the properties of the start screen

    def game_start(self):
        font = pygame.font.Font('freesansbold.ttf', 64)
        font1 = pygame.font.Font('freesansbold.ttf', 32)
        self.screen.fill(BLACK)
        start = font.render("Cross The River", True, WHITE)
        self.screen.blit(start, (150, 275))
        enter = font1.render("Press Enter To Continue", True, WHITE)
        self.screen.blit(enter, (250, 740))
        pygame.display.flip()
        self.wait_for_key()

    # defining the properties of the start screen

    def gameover(self):
        if self.playing or self.finish:
            return
        else:
            # print("abc")
            self.screen.fill(BLACK)
            font = pygame.font.Font('freesansbold.ttf', 64)
            font1 = pygame.font.Font('freesansbold.ttf', 32)
            game_over = font.render("Gameover", True, WHITE)
            if self.player1.status == 1:
                winner = font1.render("Player2 Wins", True, WHITE)
            if self.player2.status == 1:
                winner = font1.render("Player1 Wins", True, WHITE)
            if self.player1.status == 0 and self.player2.status == 0:
                if self.player1.score > self.player2.score:
                    winner = font1.render("Player1 Wins", True, WHITE)
                elif self.player1.score == self.player2.score:
                    winner = font1.render("Tied Up", True, WHITE)
                else:
                    winner = font1.render("Player2 Wins", True, WHITE)
            enter = font1.render("Press Enter To Continue", True, WHITE)
            self.screen.blit(enter, (250, 740))
            self.screen.blit(game_over, (250, 275))
            self.screen.blit(winner, (310, 350))
            pygame.display.flip()
            self.wait_for_key()

    # This function gives the player control over continuing the game or exiting

    def wait_for_key(self):
        waiting = True
        while waiting:
            self.clock.tick(60)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    waiting = False
                    self.finish = True

                pressed = pygame.key.get_pressed()
                if pressed[pygame.K_RETURN]:
                    waiting = False

    def newlevel(self):
        # while loop to ensure continuous execution of the code until a
        # particular event forces otherwise
        while self.playing:
            # ensuring that the game ends after the 6th level

            if (self.player1.level == 5) and (self.player2.level == 5):
                self.playing = False

            # Game over when One player dies but the other does not

            if (self.player1.status ^ self.player2.status) == 1 and (
                    self.player1.level != self.player2.level):
                self.playing = False
            if self.player1.status and self.player2.status:
                self.player1.status = 0
                self.player2.status = 0
                self.player1.posx = self.player1.tempx
                self.player1.posy = self.player1.tempy
                self.player2.posx = self.player2.tempx
                self.player2.posy = self.player2.tempy
                self.player1.level += 1
                self.player2.level += 1
                self.count = 0

            self.screen.fill(ROYAL_BLUE)
            if not self.done:
                self.mainplay = self.player1
            else:
                self.mainplay = self.player2
            self.mainplay.crocxspeed = 1 + self.mainplay.level
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.finish = True
                    self.playing = False
                # reducing the score of teh player as the time increases
                if event.type == self.scorereduce:
                    self.mainplay.score -= 4

            # Controls for the movement of the player
            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_UP]:
                self.mainplay.posy -= 2
            if pressed[pygame.K_DOWN]:
                self.mainplay.posy += 2
            if pressed[pygame.K_LEFT]:
                self.mainplay.posx -= 2
            if pressed[pygame.K_RIGHT]:
                self.mainplay.posx += 2
            # movement of the crocodile and collision definitions
            if self.count == 0:
                for i in range(5):
                    self.crocxchange[i] = self.mainplay.crocxspeed
                self.count = 1

            for i in range(5):
                self.crocx[i] += self.crocxchange[i]
                if self.crocx[i] <= 0:
                    self.crocxchange[i] = +self.mainplay.crocxspeed
                if self.crocx[i] >= 760:
                    self.crocxchange[i] = -self.mainplay.crocxspeed
                self.collision = self.collide(self.crocx[i], self.crocy[i], 35)
                if self.mainplay.count == 0:
                    if (self.crocy[i] < self.mainplay.posy) and (
                            not self.mainplay.counter[i]) and (
                            not self.collision):
                        self.mainplay.counter[i] = 1
                        self.mainplay.score += 10
                else:
                    if (self.crocy[i] > self.mainplay.posy) and (
                            not self.mainplay.counter[i]) and (
                            not self.collision):
                        self.mainplay.counter[i] = 1
                        self.mainplay.score += 10
                if self.collision:
                    pygame.time.delay(500)
                    self.game_over += 1
                    self.done = not self.done
                    self.mainplay.status = 1

            for i in range(6):
                self.collision = self.collide(self.dangerx[i], self.dangery[i],
                                              40)
                if self.mainplay.count == 0:
                    if (self.dangery[i] < self.mainplay.posy) and (
                            not self.mainplay.counterdang[i]) and (
                            not self.collision):
                        self.mainplay.counterdang[i] = 1
                        self.mainplay.score += 5
                else:
                    if (self.dangery[i] > self.mainplay.posy) and (
                            not self.mainplay.counterdang[i]) and (
                            not self.collision):
                        self.mainplay.counterdang[i] = 1
                        self.mainplay.score += 5
                if self.collision:
                    pygame.time.delay(500)
                    self.game_over += 1
                    self.done = not self.done
                    self.mainplay.status = 1

            for i in range(24):
                self.collision = self.collide(self.stoneX[i], self.stoneY[i],
                                              47)
                if self.collision:
                    # self.game_over += 1
                    pygame.time.delay(500)
                    self.done = not self.done
                    self.mainplay.status = 1

            if self.mainplay.posx <= 0:
                self.mainplay.posx = 0

            if self.mainplay.posx >= 760:
                self.mainplay.posx = 760

            if self.mainplay.posy <= 0:
                self.mainplay.posy = 0
                if self.mainplay.count:
                    self.player1.counter = [0] * 5
                    self.player2.counter = [0] * 5
                    self.player1.counterdang = [0] * 6
                    self.player2.counterdang = [0] * 6
                    self.mainplay.level += 1
                    self.count = 0
                    self.done = not self.done
                    self.mainplay.count = not self.mainplay.count
                    self.mainplay.change()

            if self.mainplay.posy >= 716:
                self.mainplay.posy = 716
                if not self.mainplay.count:
                    self.player1.counter = [0] * 5
                    self.player2.counter = [0] * 5
                    self.player1.counterdang = [0] * 6
                    self.player2.counterdang = [0] * 6
                    self.mainplay.level += 1
                    self.count = 0
                    self.done = not self.done
                    self.mainplay.count = not self.mainplay.count
                    self.mainplay.change()

            self.dang()
            self.recta()
            self.croc()
            self.stone()
            self.player(self.mainplay)
            self.score(self.mainplay, 20, 10, 630, 10)
            pygame.display.flip()


# properties of each player is defined in the class

class Player:
    def __init__(self, x, y, id, name):
        self.name = name
        self.posx = x
        self.posy = y
        self.count = id
        self.counter = [0] * 5
        self.counterdang = [0] * 6
        self.tempx = self.posx
        self.tempy = self.posy
        self.boatimg = pygame.image.load('boatmain21.png')

    def change(self):
        if self.count:
            self.boatimg = pygame.image.load('boatmain21.png')
        else:
            self.boatimg = pygame.image.load('boatmain2.png')

    score = 0
    level = 0
    status = 0


g = Game()
g.game_start()
# print(g)
while not g.finish:
    g.new_game()
    g.gameover()
pygame.quit()
